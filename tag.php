<?php
require_once 'blog.php';
$conn = db_get_connection(); 
if (isset($_GET["sort"])) {
  $sort = $_GET["sort"];    
} else {
  $sort = "DESC";
}                 
if (isset($_GET['pageno2'])) {
  $pageno2 = $_GET['pageno2'];
} else {
 $pageno2 = 1;
}
$tagidvalue2 = $_GET["tag"];               
$n = 2;
$offset = ($pageno2 - 1) * $n;
$total_pages = page_counter($n, $conn);                  
$data = get_posts_by_tagid($conn, $offset, $n, $sort, $tagidvalue2);
?>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="category.php">Manage Categories</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Tag Posts</h1>
            <!-- <span class="subheading">A Blog Theme by Start Bootstrap</span> -->
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
      <center>
        <div class="btn btn-dark">
          <?php  
          $td = $_GET['tag'];
          if(isset($_GET['pageno2'])) {
            $pno = $_GET['pageno2'];
          }
          else{
            $pno = 1;   
          }     
          ?>

          <form name="frm1" method="GET" >
            <input type="hidden" name="tag" value="<?php echo $td; ?>">
            <input type="hidden" name="pageno2" value="<?php echo $pno; ?>">
            <select  name="sort" onchange="this.form.submit()">
              <option value="">select order</option>
              <option value="DESC">Desending orderby date</option>
              <option value="ASC">Asending orderby date</option>
            </select>     
          </form>
        </div>
      </center>
         
      <?php
      if (isset($data)) {                
        foreach ($data as $row) {
          $val = $row["bid"];
          $str = $row["content"];
          $cont =  content_trimmer($str);                       
          echo '
                    <div class="post-preview"> 
                      <a href="sql.php?id='.$val.'">
                      <h2 class="post-title">'.$row["title"].'</h2>
                      <h3 class="post-subtitle">'.$cont.'</h3>
                      </a><p class="post-meta">Posted by
                      <a href="#">Start Bootstrap</a>
                      on '.$row["date"].'</p>    
                      ';
          $data2 = get_tags_by_blogid($val, $conn);
          echo "<p>Tags: ";
          if (isset($data2)) { 
            foreach ($data2 as $row2) {
              $tagidval2 = $row2["tid"];
                echo '<a href="tag.php?tag='.$tagidval2.'">#'.$row2["tags"].' </a>';             
            }
          }

          $cat = get_categories_by_blogid($val,$conn); 

          echo "<p>Category: ";
          if (isset($cat)) { 
            foreach ($cat as $row2) {
              $catidval = $row2["cid"];
              echo '<a href="catposts.php?cat=' . $catidval . '">#' . $row2["categories1"] . ' </a>';
              echo " ";
            }
          }


          echo "</p>
                     </div>
                     <hr>";
        }
      } 
      else {
           echo "0 results";
      }         
      $conn = NULL;
      ?>
     

  <hr>


   <!-- Pager -->
   <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if($pageno2 == 1){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if($pageno2 == 1){ echo '#'; } else { echo "tag.php?tag=$tagidvalue2&sort=$sort&pageno2=".($pageno2 - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if($pageno2 == $total_pages){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if($pageno2 == $total_pages){ echo '#'; } else { echo "tag.php?tag=$tagidvalue2&sort=$sort&pageno2=".($pageno2 + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--pager-->

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
