<?php 

function db_get_connection() {  
  require_once "config.php";
  static $db;
  try {
    if (!isset($db)) {
      $db = new PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
  } catch(PDOException $ex) {
      echo "Connection failed! " . $ex->getMessage();
  }
  return $db;
}



function db_tables_creation($dbname, $username, $password, $dummyno) {
  try {
    $conn1 = new PDO("mysql:host=localhost", $username, $password);
    $conn1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "DROP DATABASE IF EXISTS " . $dbname . ";";
    $conn1->exec($sql);
    $db = "CREATE DATABASE " . $dbname;
    $usedb = "USE " . $dbname; 
    $conn1->exec($db);
    $conn1->exec($usedb);

    $table1 = "CREATE TABLE blog (
              bid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              title TEXT NOT NULL,
              content TEXT NOT NULL,
              date DATE NOT NULL)";    
    $conn1->exec($table1);    

    $table2 = "CREATE TABLE tag (
              tid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              tags varchar(100) NOT NULL UNIQUE)";    
    $conn1->exec($table2);

    $table3 = "CREATE TABLE reltab (
               blogid INT(11) NOT NULL,
              tagid INT(11) NOT NULL,
              FOREIGN KEY (blogid) REFERENCES blog (bid),
              FOREIGN KEY (tagid) REFERENCES tag (tid))";
    $conn1->exec($table3);      

    $table4 = "CREATE TABLE categories (
              cid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              categories1 varchar(100) NOT NULL UNIQUE)"; 
    $conn1->exec($table4);

    $table5 = "CREATE TABLE catrel (
                blogid INT(11) NOT NULL,
                catid INT(11) NOT NULL,
                FOREIGN KEY (blogid) REFERENCES blog(bid),
                FOREIGN KEY (catid) REFERENCES categories(cid))";
    $conn1->exec($table5);

    if (isset($_POST['dummynumber'])) {
      for ($k = 0; $k < number_format($dummyno); $k++) {
        $dummycurrentdate = date("Y-m-d H:i:s");
        $dummytitle = generate_dummy(4);
        $dummyblog = generate_dummy(100);
        $dummytag = generate_dummy(2);
        $dummycat = generate_dummy(1);
        $words = explode(" ", $dummyta);
        $s = sizeof($words);  
        //$catsize = sizeof($cat);
        for($i = 0; $i < $s; $i++) {
          $words[$i] = trim($words[$i]);
        }

       add_post($conn1, $dummytitle, $dummyblog, $dummytag, $dummycurrentdate,$dummycat);	
        
      }
    }
    //header("location:index.php");
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
}


function add_category($conn, $category) {
  $category = strtolower($category);
  try {
    $sqlcat = "INSERT INTO categories(categories1) VALUES ('$category')";
    $conn->exec($sqlcat);

  } catch(PDOException $e) {
    //echo "Connection failed: " . $e->getMessage();
  }
}


function get_all_categories($conn)	{
  $stmt = $conn->prepare("SELECT cid, categories1 FROM categories ORDER BY cid");                 
  $stmt->execute();
  $data = $stmt->fetchAll();
  return $data;	
  }


function add_post($conn, $title, $blogg, $tag, $currentdate,$cat)	{
    $tag = trim($tag, " ");
    $tag = strtolower($tag);
    $words = explode(" ", $tag);
    $s = sizeof($words);


    $catsize = sizeof($cat);
    for ($i = 0; $i < $s; $i++) {
      $words[$i] = trim($words[$i]);
    }
    try { 
      $blogg = addslashes($blogg);      
      if( $title !="") {

        $sql = "INSERT INTO blog (title, content, date) VALUES ('$title', '$blogg', '$currentdate')";
        $conn->exec($sql);
  
        for ($i =0; $i < $s; $i++) {
          $resl = $conn->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");
          $count = $resl->rowCount();
          if ($count == 0) {
            $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
            $conn->exec($tagsq);
          }
        $relquery = "INSERT INTO reltab(blogid, tagid)
        SELECT bbid.bid, ttid.tid
        FROM blog bbid JOIN	tag ttid
        ON bbid.title = '$title' AND ttid.tags = '$words[$i]'";
        $conn->exec($relquery);
        }



        ///////////////////////////////////////////////////

          $resl1 = $conn->query("SELECT  categories1 FROM categories  WHERE  categories1 = '$cat'");      
          $count1 = $resl1->rowCount();   
         
          if ($count1==0) {
            $catsq="INSERT INTO categories (categories1)  VALUES('$cat')";
            $conn->exec($catsq);
          }
////////////////////////////////////////////////////////////////////////////////



          if (is_array($cat)) {
            for ($i =0; $i < $catsize; $i++) {
              $catrelquery = "INSERT INTO catrel(blogid, catid)
                              SELECT bbid.bid, ccid.cid
                              FROM blog bbid JOIN categories ccid
                              ON bbid.title = '$title' AND ccid.categories1 = '$cat[$i]'";
              $conn->exec($catrelquery);       
            }
          } else {
            $catrelquery = "INSERT INTO catrel(blogid, catid)
                            SELECT bbid.bid, ccid.cid
                            FROM blog bbid JOIN categories ccid
                            ON bbid.title = '$title' AND ccid.categories1 = '$cat'";
            $conn->exec($catrelquery); 
          }
      }
      return TRUE;
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
  }  


  function generate_dummy($n){
    $b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
            'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
            'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
            'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
            'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
            'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
            'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
            'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
            'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
            'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
            'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
            'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
            'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
            'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
            'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
            'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
            'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
            'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
            'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
            'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
            'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
            'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
            'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
            'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
            'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
            'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
            'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
            'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
            'vulputate',
    );
        
    $rand=array(); 
    shuffle($b);    
    for($i=0;$i<$n;$i++){
        $rand[$i]=$b[$i];
    }
    $rand= implode(" ",$rand);
    return $rand;
  }

  function page_counter($n, $conn)	{
    $total_pages_sql = "SELECT bid FROM blog";
    $q1 = $conn->query($total_pages_sql);
    $total_rows = $q1->rowCount();
    $total_pages = ceil($total_rows / $n);
    return $total_pages;
  }

  function content_trimmer($str)	{
    $words = explode(" ", $str);
    $cont =  implode(" ", array_splice($words, 0, 200));
    if (str_word_count($cont) > 199) {
      $cont= $cont."...";
    }
    return $cont;
  }

  function get_tags_by_blogid($idval, $conn)	{
    $sql1 = "SELECT tag.tags, tag.tid FROM reltab, tag 
              WHERE reltab.blogid = ? AND tag.tid = reltab.tagid";
    $stmt2 = $conn->prepare($sql1);
    $stmt2->execute([$idval]);
    return $stmt2->fetchAll();
    }

  function get_categories_by_blogid($idval, $conn)	{
    $sql1 = "SELECT categories.categories1, categories.cid FROM catrel, categories 
              WHERE catrel.blogid = ? AND categories.cid = catrel.catid";
    $stmt2 = $conn->prepare($sql1);
    $stmt2->execute([$idval]);
    return $stmt2->fetchAll();
    } 

  
  function get_all_posts($conn, $offset, $n, $sort)	{
      $stmt = $conn->prepare("SELECT bid, title, content, date FROM blog 
        ORDER BY bid $sort LIMIT $offset, $n");                 
      $stmt->execute();
      $data = $stmt->fetchAll();
      return $data;	
      }

  function get_post_by_blogid($conn, $id)	{
    $stmt = $conn->prepare("SELECT  title , content, date FROM  blog WHERE bid=?");
    $stmt->execute([$id]); 
    return 	$stmt->fetch();
    }

  function get_posts_by_categoryid($conn, $offset, $n, $sort, $catidvalue2)	{            
      $sql = "SELECT bid, title, content, date FROM blog, catrel 
              WHERE blog.bid = catrel.blogid and $catidvalue2 = catrel.catid 
              ORDER BY bid $sort LIMIT $offset, $n"; 
  
  
      if (filter_var($catidvalue2, FILTER_VALIDATE_INT)) {
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $data = $stmt->fetchAll();
      }
      return $data;
    } 

  function get_posts_by_tagid($conn, $offset, $n, $sort, $tagidvalue2)	{
      $sql = "SELECT bid, title, content, date FROM blog, reltab 
              WHERE blog.bid = reltab.blogid and $tagidvalue2 = reltab.tagid 
              ORDER BY bid $sort LIMIT $offset, $n";                   
      if (filter_var($tagidvalue2, FILTER_VALIDATE_INT)) {
        $stmt = $conn->prepare($sql); 
        $stmt->execute();
        $data = $stmt->fetchAll();
      }
      return $data;
    }

    function update_post_by_blogid($conn, $title, $blogg, $tag, $idval, $cat) {
      $tag = trim($tag, " ");
      $tag = strtolower($tag);
      $words = explode(" ", $tag);
      $s = sizeof($words);
      $catsize = sizeof($cat);
      for ($i = 0; $i < $s; $i++) {
        $words[$i] = trim($words[$i]);
      }
      try {  
        $blogg = addslashes($blogg);
        $sql = "UPDATE blog SET title = '$title', content = '$blogg' WHERE bid = $idval";
        $conn->exec($sql);
        $del = "DELETE FROM reltab
         where blogid = $idval";
        $conn->exec($del);
        $del1 = "DELETE FROM catrel
          WHERE blogid = $idval";
        $conn->exec($del1);
  
        for ($i = 0; $i < $s; $i++) {
          $resl = $conn->query("SELECT tags FROM tag  WHERE  tags = '$words[$i]'");
          $count = $resl->rowCount();    
          if ($count == 0) {
            $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
            $conn->exec($tagsq);
          }
          $relquery = "INSERT INTO reltab (blogid, tagid)
                        SELECT bbid.bid, ttid.tid
                        FROM blog bbid JOIN	tag ttid
                        ON bbid.bid = $idval AND ttid.tags = '$words[$i]'";
          $conn->exec($relquery);
        }
  
  
        for ($i =0; $i < $catsize; $i++) {
          $catrelquery = "INSERT INTO catrel(blogid, catid)
                          SELECT bbid.bid, ccid.cid
                          FROM blog bbid JOIN categories ccid
                          ON bbid.title = '$title' AND ccid.categories1 = '$cat[$i]'";
          $conn->exec($catrelquery);
    
          }
        return TRUE;  
      }catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
      }
    }

    function delete_post_by_blogid($conn, $idval) {
      try {  
        $reldel = "DELETE FROM reltab WHERE blogid = $idval";
        $conn->exec($reldel);
        $catreldel = "DELETE FROM catrel WHERE blogid = $idval";
        $conn->exec($catreldel);
        $blogdel = "DELETE FROM blog WHERE bid = $idval";
        $conn->exec($blogdel);
  
  
        return TRUE;  
      }catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
      }
    } 

    function get_category_by_categoryid($conn, $id) {
      $stmt = $conn->prepare("SELECT  cid , categories1 FROM  categories WHERE cid=?");
      $stmt->execute([$id]); 
      return 	$stmt->fetch();    
    }

    function update_category_by_categoryid($conn, $category, $idval) {
      try {  
  
        $sql = "UPDATE categories SET categories1 = '$category' WHERE cid = $idval";
        $conn->exec($sql);
        return TRUE;  
      }catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
      }
    }

    function delete_category_by_categoryid($conn, $idval) {
      try {  
  
        $catdel = "DELETE FROM categories WHERE cid = $idval";
        $conn->exec($catdel);
        return TRUE;  
      }catch(PDOException $e) {
        //echo "Connection failed: " . $e->getMessage();
        // echo '<script language="javascript">';
        // echo 'alert("Sorry, Category cannot be deleted as it is refered to some blog posts")';  //not showing an alert box.
        // echo '</script>';
        // header("Location:category.php");
        echo '<script language="javascript">';
        echo 'alert("Sorry, Category cannot be deleted as it is refered to some blog posts")';
        echo '</script>';
      }
    } 



    function get_categories_by_blogid1($idval, $conn)	{
      $sql1 = "SELECT categories.categories1, categories.cid FROM catrel, categories 
                WHERE catrel.blogid = ? AND categories.cid = catrel.catid";
      $stmt2 = $conn->prepare($sql1);
      $stmt2->execute([$idval]);
      return $stmt2->fetchAll();
      } 





  function category_selected($conn , $idval, $cid)	{
    $sql = "SELECT blogid FROM catrel WHERE blogid = '$idval' AND catid = '$cid';";
    $res = $conn->query($sql);
    $res->setFetchMode(PDO::FETCH_ASSOC);
    return $res;	
  }


?>  