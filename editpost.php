<?php

require_once 'blog.php';

$conn = db_get_connection();
$categoryview = get_categories_by_blogid($conn);
$idval = $_GET["id"];
if(filter_var($idval, FILTER_VALIDATE_INT)){
  $row = get_post_by_blogid($conn, $idval);
}
$data2 = get_tags_by_blogid($idval, $conn);                 
if (isset($_POST['updateblog'])) {
  if (isset($_POST['title'])) {
  	$title = $_POST['title'];
  }
	if (isset($_POST['blog'])) {
  	$blogg = $_POST['blog'];
  }
  if (isset($_POST['tags'])) {
  	$tag = $_POST['tags'];
  }
  if(isset($_POST['catlist'])) { 
    $cat = $_POST['catlist'];
  }
  $statusedit = update_post_by_blogid($conn, $title, $blogg, $tag, $idval,$cat);
  if (isset($statusedit)) {
    header("location:sql.php?id=$idval&status=1");
  }
}
?>



<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="category.php">Manage Categories</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/blog-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Add your blog here</h1>
            <span class="subheading">Lets start blogging...</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p>Fill out the form to add your blog post</p>
        
        <form name="blogform"  method="POST" action="">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Title</label>
              <input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo $row['title']; ?>"
                    required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>

          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Blog</label>
              <textarea rows="10" class="form-control" placeholder="Your Blog Content" name="blog" 
              required data-validation-required-message="Please type your content."><?php echo $row['content']; ?></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>

          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Tags</label>
              <textarea rows="3" class="form-control" placeholder="Tags" name="tags" required data-validation-required-message="Please type your content.">
              <?php
              if (isset($data2)) { 
                foreach ($data2 as $row2) {
                	echo $row2["tags"] . " ";        
                }
              }
              ?>
            </textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>

          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Categories</label>
                <!--select name = "catlist[]" multiple = "multiple"> size = 10 --> 
                <select id="selectcategory" name = "catlist[]" multiple = "multiple" size = 5> 
                  <!--?php foreach ($categoryview as $row) { 

                  $cid = $row['cid'];
                  $res = category_selected($conn , $idval, $cid);
                  $count = $res->rowCount();
                  echo '<label for="one"> ';
                  if ($count == 0) {
                    echo '<option value="'.$row["categories1"].'"  >'.$row["categories1"].'</option>';
                 } else {               
                   echo '<option value="'.$row["categories1"].'"  selected="selected">'.$row["categories1"].'</option>';
                 }
                 }
                 ?-->
                  <?php foreach ($categoryview as $row) { 

                    $cid = $row['cid'];
                    $res = category_selected($conn , $idval, $cid);
                    $count = $res->rowCount();
                    
                    echo '<label for="one"> ';
                    if ($count == 0) {
                      echo '<option value="'.$row["categories1"].'"  >'.$row["categories1"].'</option>';
                    } else {               
                      echo '<option value="'.$row["categories1"].'"  selected="selected">'.$row["categories1"].'</option>';
                    }
                  }
                  ?>
                </select>

              <p class="help-block text-danger"></p>
            </div>
          </div>

          <br>
          <div id="success"></div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="updateblog" id="UpdateBlogButton">Update</button>
          </div>
        </form>
      
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>




