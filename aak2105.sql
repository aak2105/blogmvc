-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 25, 2019 at 06:26 PM
-- Server version: 5.7.28-0ubuntu0.16.04.2
-- PHP Version: 7.0.33-0ubuntu0.16.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aak2105`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `bid` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`bid`, `title`, `content`, `date`) VALUES
(1, 'vivamus mollis commodo class', 'justo ad volutpat natoque mattis ut cras et ac tincidunt maecenas primis ante aliquet varius taciti neque mollis viverra aptent torquent suspendisse imperdiet sed maximus massa convallis arcu pellentesque magnis sapien vitae diam conubia per potenti habitasse pharetra feugiat fames dis ullamcorper adipiscing ipsum orci accumsan laoreet morbi semper pulvinar cubilia litora dapibus risus nostra tempus lacinia faucibus donec inceptos consequat vestibulum finibus eros fringilla porta dictum rhoncus sodales curae sollicitudin himenaeos auctor nascetur vulputate hac mauris iaculis erat platea congue purus odio sagittis porttitor magna vivamus senectus euismod phasellus eleifend vel sociosqu vehicula class malesuada dolor elit posuere mus', '2019-11-25'),
(2, 'turpis taciti eleifend condimentum', 'himenaeos urna habitant magnis condimentum tristique rutrum nulla ultricies pulvinar penatibus faucibus pharetra egestas consectetur malesuada aliquam mus congue cubilia torquent fermentum nisl euismod lobortis vulputate sem nunc posuere libero curabitur viverra augue fusce inceptos nostra tellus neque platea suspendisse purus tortor mi mauris non sapien cras eget adipiscing suscipit ridiculus molestie efficitur porta fringilla iaculis litora curae class dolor ad risus sagittis semper varius mollis bibendum dis primis convallis quis lorem donec sollicitudin etiam ipsum facilisi id a odio gravida quam aenean hendrerit turpis habitasse enim commodo natoque at sed justo est diam magna netus volutpat ligula potenti vel', '2019-11-25'),
(3, 'congue nisi ligula condimentum', 'pretium convallis platea vehicula turpis libero netus accumsan ex pellentesque ante gravida sodales cubilia nulla ultricies duis ultrices justo fames lacus tempus aptent penatibus lobortis feugiat mus hendrerit tincidunt habitant lorem vestibulum rhoncus lectus hac suscipit nibh lacinia neque mattis aliquam ridiculus sapien class malesuada nec felis montes erat varius ornare aliquet primis taciti sed quisque natoque maximus at eros odio placerat ipsum morbi dis fringilla dui curae dictum laoreet integer facilisi et nostra diam tellus bibendum massa mollis consectetur sit aenean semper luctus venenatis sollicitudin pharetra nunc sociosqu metus imperdiet consequat commodo dapibus maecenas nisi ac est ut himenaeos', '2019-11-25'),
(4, 'imperdiet malesuada tincidunt curae', 'porta sapien imperdiet praesent at ultricies eros sem vivamus curae fringilla fames tempus mollis efficitur ex suspendisse eleifend luctus elementum consectetur facilisis montes accumsan ullamcorper lacinia ipsum diam est nibh facilisi justo tristique tortor amet purus quisque etiam per integer nisl pellentesque rutrum quis semper sed non condimentum nam elit aliquet duis vehicula adipiscing blandit hac natoque cras proin maecenas fusce neque ligula euismod quam hendrerit metus auctor eu lobortis ut turpis ultrices egestas et nascetur class odio rhoncus vel libero aenean dapibus scelerisque sodales sociosqu dignissim urna phasellus pulvinar pharetra mi magnis habitasse enim donec volutpat felis himenaeos dui', '2019-11-25'),
(5, 'curae nec vestibulum aliquam', 'vehicula sed semper aptent lectus sem pulvinar a euismod cubilia nam laoreet malesuada sagittis ipsum habitasse montes consequat nisl rhoncus consectetur rutrum per dui varius aliquam suscipit in vitae adipiscing parturient dapibus nec velit bibendum convallis pellentesque litora taciti purus ridiculus morbi risus lacus suspendisse sodales porttitor mattis mi molestie senectus vel ac elit tempus maecenas dictum fermentum primis justo elementum aenean arcu dignissim commodo eros urna ligula at tempor porta cursus dis eleifend ultrices blandit sit ad viverra scelerisque libero facilisi gravida proin donec erat hendrerit ultricies neque venenatis felis ante id placerat nunc quam enim tincidunt et integer', '2019-11-25'),
(6, 'dolor magna tellus massa', 'consequat taciti mauris ultricies dui auctor curae senectus ante porttitor turpis iaculis tempor aptent molestie platea neque commodo class sed hac viverra rhoncus suspendisse nulla eu fames sagittis felis feugiat lacinia purus lectus laoreet egestas rutrum risus velit condimentum augue tristique facilisi luctus eleifend imperdiet elementum nibh integer orci torquent phasellus potenti hendrerit fusce porta tempus ex convallis dictum magna aenean erat pulvinar ipsum ultrices natoque morbi in efficitur odio venenatis nisi volutpat nostra montes nisl libero habitant lacus at nunc facilisis dapibus ridiculus sem donec sollicitudin sit congue himenaeos leo vulputate maximus quisque scelerisque magnis etiam curabitur per sodales', '2019-11-25'),
(7, 'fringilla pulvinar vel nulla', 'integer primis curae fringilla mauris etiam curabitur tincidunt gravida interdum iaculis erat facilisis metus varius donec mus id justo vestibulum imperdiet ligula risus enim sodales aptent potenti sem dis morbi tempus parturient mattis hac penatibus est purus nulla senectus urna nostra hendrerit porttitor posuere phasellus semper magna nisl ornare neque luctus ante vehicula diam pellentesque egestas viverra elementum suspendisse scelerisque cursus libero conubia blandit condimentum nunc ac mi lacus nec vivamus mollis finibus aliquet eleifend suscipit sollicitudin fames duis rutrum turpis facilisi aenean nibh proin massa non feugiat commodo convallis torquent augue orci montes auctor pretium consequat ullamcorper nascetur sed', '2019-11-25'),
(8, 'penatibus nibh ligula habitasse', 'purus maecenas quam bibendum potenti natoque ad risus curabitur euismod pellentesque nec litora lacus libero enim tincidunt vestibulum dui tempus nullam metus interdum id etiam hendrerit curae finibus class conubia ornare dolor mattis et massa a at eu parturient taciti ex ac fringilla pulvinar imperdiet senectus suspendisse proin eros torquent fermentum varius consectetur dapibus fusce iaculis aliquam ligula nunc in nisi volutpat platea venenatis eleifend luctus scelerisque nostra sapien odio hac molestie dignissim vel orci convallis rhoncus duis posuere justo himenaeos semper malesuada cras diam turpis congue efficitur augue dis porttitor erat blandit ultricies auctor arcu ipsum nascetur vehicula pretium', '2019-11-25'),
(9, 'a vitae lacus nostra', 'dapibus diam cras sagittis augue ipsum venenatis ante per molestie netus a sodales bibendum lacinia porta morbi lacus dis risus justo gravida velit scelerisque nullam fames nec rhoncus commodo aptent posuere mauris habitant condimentum praesent mus maximus ligula pharetra non duis ad interdum cursus taciti quam leo placerat efficitur consectetur eleifend dignissim parturient tellus ac facilisis adipiscing mi conubia ex pulvinar donec vestibulum fermentum elit malesuada est euismod imperdiet purus finibus sit nascetur nisi penatibus luctus sociosqu viverra habitasse torquent urna orci vulputate congue dictum magna hendrerit aliquam odio himenaeos potenti dui tempor litora volutpat consequat ultricies vel lobortis pretium', '2019-11-25'),
(10, 'ultrices sed magna sodales', 'purus fermentum platea leo nec lorem congue placerat nulla et sed semper proin ornare aptent montes vel mauris nascetur neque eu senectus justo at magna taciti vivamus sodales aliquam conubia consequat metus gravida turpis etiam suscipit habitasse finibus porttitor dis bibendum lacus sociosqu fringilla non nam ex parturient tristique lacinia dictumst tempus hac est facilisi dolor felis ad curae tempor eget dui netus pretium vulputate nostra per ac adipiscing in mollis nisi suspendisse himenaeos egestas phasellus natoque torquent condimentum consectetur interdum feugiat sapien id varius rhoncus maximus auctor quam venenatis iaculis scelerisque aenean maecenas amet dignissim commodo curabitur inceptos donec', '2019-11-25'),
(11, 'metus libero ad fermentum', 'venenatis hac varius pulvinar proin neque aptent rutrum erat torquent bibendum malesuada nascetur ridiculus curabitur donec suscipit diam faucibus fames egestas posuere aenean efficitur rhoncus lorem mi euismod magnis consectetur consequat sociosqu nec iaculis hendrerit ullamcorper leo convallis sollicitudin aliquam dapibus lacinia mattis cubilia ornare augue ac fusce nulla penatibus senectus facilisis eget natoque inceptos vivamus commodo eu luctus integer class cras at maecenas himenaeos taciti libero adipiscing lobortis conubia habitant tempor primis tortor tellus ut dui sed quam maximus dis lectus tristique ipsum vestibulum justo pharetra dictumst vehicula nunc risus ex elementum semper placerat velit sem nisi ad morbi', '2019-11-25'),
(12, 'enim rutrum mauris euismod', 'duis turpis volutpat aliquet tempor ornare lacus feugiat scelerisque vivamus torquent aptent tellus taciti praesent curae eget accumsan facilisis dictum maecenas dictumst egestas nullam platea penatibus porta inceptos cubilia arcu lobortis erat dapibus nibh molestie montes id proin posuere amet quis mauris hendrerit pharetra blandit laoreet condimentum diam libero dignissim ex lacinia senectus convallis nulla porttitor aliquam urna varius pretium parturient est conubia metus adipiscing potenti imperdiet integer ac consectetur sed a luctus ultricies eleifend netus rutrum facilisi primis sociosqu placerat nisi ad felis non natoque ligula neque lorem consequat etiam congue habitasse gravida tristique morbi pellentesque commodo faucibus sit', '2019-11-25'),
(13, 'lacinia at volutpat nec', 'curae vel phasellus nulla vestibulum conubia bibendum non pharetra netus sed senectus leo magnis lobortis platea nunc libero erat ex dapibus aliquam montes ultrices molestie lectus urna massa duis quis velit quam pulvinar dui mus mattis malesuada consectetur nascetur parturient ridiculus finibus a eu augue aptent sit lorem ligula facilisi consequat feugiat neque risus euismod enim elit sagittis venenatis vivamus cubilia odio pellentesque orci efficitur dignissim ornare accumsan ipsum volutpat aliquet vehicula viverra purus lacinia nibh mollis blandit pretium diam commodo posuere nisl vulputate sem proin auctor himenaeos ante congue sociosqu elementum fermentum suscipit habitant eros etiam ad maecenas nullam', '2019-11-25'),
(14, 'accumsan ridiculus gravida facilisis', 'efficitur gravida ultrices sit nisi fames nec ac morbi tristique nibh leo ultricies eget porttitor sem posuere iaculis proin dictumst metus arcu fermentum convallis parturient phasellus litora facilisis condimentum nostra ex lacinia velit primis cubilia nascetur mauris mi interdum hac aptent a integer viverra elit maecenas senectus ut molestie curabitur dictum non vitae torquent id faucibus est eleifend fringilla sodales diam aenean sagittis mus ligula cras suspendisse felis quisque feugiat et vehicula ipsum neque volutpat urna imperdiet curae rutrum enim sapien varius cursus erat habitant purus vel porta natoque ullamcorper praesent montes pellentesque rhoncus duis dapibus dui in ante consequat', '2019-11-25'),
(15, 'dis et mi accumsan', 'ullamcorper sagittis blandit aliquam elit dictumst praesent efficitur ut vivamus ante porttitor finibus ex laoreet potenti mus ad nisi phasellus habitant quis enim egestas himenaeos felis netus erat aptent rutrum est cras tellus velit eget purus etiam pretium ridiculus dis nec in lacinia luctus risus vulputate venenatis sit sociosqu parturient maximus placerat nulla suspendisse magna lobortis nam orci fames massa penatibus proin posuere integer gravida condimentum ornare faucibus vestibulum sed sapien nunc montes aenean mattis maecenas pharetra suscipit imperdiet amet leo non torquent taciti platea viverra litora dui a id porta senectus semper nostra per vitae dictum consequat eros sem', '2019-11-25'),
(16, 'mi ullamcorper nunc odio', 'ut libero pharetra neque mollis consectetur aenean aliquam venenatis sed sem rhoncus bibendum odio molestie cubilia himenaeos porttitor platea nullam parturient fames ac accumsan consequat senectus porta morbi ipsum hendrerit pulvinar tempor natoque lectus felis dictum amet proin nulla curae potenti habitant lorem magnis lacus arcu dapibus nec malesuada lacinia tincidunt vel mus erat massa quam viverra aliquet tellus faucibus per primis condimentum ex leo vitae suscipit risus maximus inceptos facilisi tortor donec placerat nostra montes justo egestas tristique velit ligula habitasse hac conubia efficitur luctus cursus eleifend gravida fringilla taciti quisque cras varius sagittis blandit pellentesque imperdiet urna nibh', '2019-11-25'),
(17, 'ut nec himenaeos penatibus', 'dis nisl cursus felis mollis mi sociosqu mauris cubilia ex habitant sed fermentum platea euismod luctus nibh quisque lectus vel fames class orci curabitur lacinia sagittis varius ante nascetur tortor ad nunc penatibus vehicula consequat maecenas eleifend massa integer venenatis in natoque scelerisque efficitur erat elementum cras potenti blandit est egestas dignissim inceptos justo risus vivamus dapibus laoreet fusce posuere praesent etiam consectetur finibus ultricies augue non nostra ac nam imperdiet taciti lobortis porta malesuada sodales dolor interdum eros eu fringilla tempor donec velit odio mattis torquent metus sem quam suspendisse magna proin sapien dictum vulputate ullamcorper dictumst urna convallis', '2019-11-25'),
(18, 'diam sagittis fusce dis', 'vulputate accumsan mattis egestas nec porta vehicula inceptos ullamcorper nisi aptent quisque mauris imperdiet posuere dui nullam quis libero natoque nunc lectus pretium habitant donec pellentesque eget risus aliquam nisl purus mi netus dis ultrices luctus consequat varius auctor vel ad erat phasellus sed facilisi interdum integer viverra ex platea consectetur elit neque ultricies eu enim sit fusce sollicitudin nam magnis a orci nulla semper gravida tristique et nibh commodo per porttitor hac proin leo massa lacinia felis nascetur tempor litora diam augue class est laoreet rhoncus efficitur tortor fringilla himenaeos arcu eros velit sodales id dolor placerat finibus ornare', '2019-11-25'),
(19, 'dictumst finibus nisi cubilia', 'semper augue morbi varius magnis donec tincidunt a nascetur egestas consectetur ex tortor lacus vehicula sollicitudin accumsan potenti sem sapien ridiculus natoque fermentum litora rhoncus purus fringilla condimentum lacinia velit placerat eros habitasse dapibus tellus at ipsum mauris faucibus vulputate justo magna adipiscing nec porta hendrerit orci nunc urna blandit ullamcorper metus nibh turpis scelerisque curabitur enim et ac sociosqu etiam aliquam ligula suspendisse himenaeos class in dis tempor dictumst auctor aenean neque curae per cursus torquent ad parturient non vel imperdiet sed facilisi sit pulvinar pellentesque nostra dolor mi pharetra mus proin phasellus sagittis libero gravida fames mollis eleifend', '2019-11-25'),
(20, 'donec tempor dictumst nulla', 'sapien posuere sit sociosqu in mus nibh himenaeos eu nostra netus magna nulla luctus dictumst lacinia placerat sagittis vehicula turpis montes libero donec tempor potenti id quisque porttitor maecenas a felis blandit faucibus taciti class fermentum venenatis consectetur suspendisse arcu elementum aliquam aliquet suscipit ut condimentum vel gravida eleifend hac iaculis quam mi vestibulum litora conubia malesuada nullam aptent leo nisi nec dolor congue ridiculus parturient dictum pharetra feugiat tellus elit erat nunc ex ultrices convallis per dignissim tortor finibus diam penatibus cras pretium integer amet purus senectus non duis scelerisque fringilla ipsum varius semper habitant facilisi viverra magnis natoque', '2019-11-25'),
(21, 'sed justo posuere magnis', 'proin placerat suspendisse mus nostra sit torquent himenaeos fames sodales cras commodo pharetra sociosqu felis donec vestibulum nulla rhoncus imperdiet libero viverra dictumst vehicula at cursus ex platea posuere elementum fringilla aliquam scelerisque sagittis auctor nisi adipiscing iaculis rutrum venenatis vel nisl penatibus tristique tempus nascetur tellus ultricies dis ridiculus finibus amet faucibus ut consectetur odio aptent metus a volutpat sapien mi etiam duis laoreet ac neque feugiat montes pulvinar tempor congue dolor lacus ante dapibus habitant vitae maximus primis quam curabitur nec ultrices dui gravida habitasse class tortor velit maecenas porttitor aliquet vulputate pretium fusce quis pellentesque efficitur in', '2019-11-25'),
(22, 'penatibus et nostra sed', 'luctus porttitor suspendisse auctor mi convallis semper tortor facilisi eget proin habitant accumsan pharetra molestie nam sociosqu placerat egestas praesent nec id laoreet at lacus nascetur montes bibendum vel vitae parturient aenean hendrerit porta vehicula massa interdum sapien sagittis mollis arcu ipsum nunc himenaeos condimentum taciti pretium imperdiet adipiscing neque litora malesuada in elementum volutpat quisque enim a dictum felis augue et euismod orci sem aliquam fusce nisi pellentesque ligula mauris nostra senectus scelerisque commodo faucibus magnis quis efficitur congue urna maximus lobortis mus ultrices est dignissim quam hac tincidunt nulla platea consectetur feugiat diam posuere curae penatibus ac class', '2019-11-25'),
(23, 'mi duis sapien magna', 'metus lacinia efficitur hendrerit aliquet dapibus fringilla ex convallis lobortis magnis pellentesque iaculis orci sagittis purus risus elementum sodales ridiculus etiam erat vehicula tellus gravida ullamcorper habitasse aenean taciti semper a eleifend tempus ultrices ut dui et odio enim nibh justo arcu libero vivamus integer venenatis turpis rhoncus donec faucibus dignissim parturient blandit varius magna felis phasellus duis viverra vulputate per quam urna lectus aliquam hac non proin nullam facilisis nisi penatibus sociosqu cubilia himenaeos nascetur porta dictumst lacus suscipit suspendisse id mattis adipiscing mollis torquent vitae est elit litora ornare nostra ligula dolor sit quis scelerisque porttitor fames tristique', '2019-11-25'),
(24, 'placerat suscipit maecenas odio', 'dui feugiat pellentesque nulla tincidunt convallis lorem class vehicula suspendisse sodales libero mauris est volutpat luctus consectetur duis morbi condimentum quam mi viverra iaculis ornare nam amet dis primis etiam ligula egestas platea lectus commodo curabitur sagittis vulputate ad lacus senectus eleifend aenean mattis urna id habitasse augue bibendum magna ut quis euismod risus netus parturient nunc habitant non arcu varius sollicitudin himenaeos massa ullamcorper nisl dignissim scelerisque natoque pharetra venenatis magnis cubilia fames velit litora sit rhoncus hac facilisi consequat porttitor finibus aliquam mollis quisque a dictumst molestie elementum conubia ultricies phasellus adipiscing turpis diam imperdiet mus dapibus enim', '2019-11-25'),
(25, 'tempor ac aliquam etiam', 'ridiculus non bibendum integer duis magnis quam parturient ipsum semper eu fringilla natoque varius egestas erat elementum sociosqu condimentum maximus velit tellus nulla curabitur mollis tempor etiam ligula suspendisse nec ad pretium tortor hendrerit amet laoreet lectus nostra adipiscing mus et arcu eget vulputate sit montes dolor felis praesent suscipit pulvinar porttitor nisi placerat purus litora curae scelerisque efficitur nam senectus magna metus malesuada primis rutrum consequat turpis mauris tempus faucibus fermentum odio ac facilisis proin convallis diam tristique nibh neque vel posuere hac lacus per morbi conubia maecenas ullamcorper facilisi ornare justo donec libero id nascetur finibus aliquam pellentesque', '2019-11-25'),
(26, 'efficitur dictumst sed at', 'lacus pretium ridiculus conubia habitasse elementum nam placerat dictumst eget mollis efficitur odio ante purus metus sed pulvinar suspendisse convallis massa sit fames ultricies adipiscing amet tincidunt curae donec dignissim congue ultrices consequat egestas felis eros in feugiat hendrerit lacinia aliquam euismod imperdiet varius tempor laoreet ornare etiam ad magna accumsan mauris sollicitudin iaculis ex sapien nulla ipsum interdum posuere dolor per nec ullamcorper velit facilisi erat eleifend eu praesent urna platea nunc morbi magnis malesuada nisl luctus porttitor fusce phasellus parturient cras vivamus vulputate lectus gravida ac libero duis habitant mus bibendum sodales enim dictum elit torquent augue maecenas', '2019-11-25'),
(27, 'montes potenti volutpat velit', 'praesent commodo consequat mollis lobortis elementum dis scelerisque ut varius class lorem placerat habitant tortor proin non est laoreet montes rutrum metus aliquam sociosqu magna facilisi cursus felis porttitor luctus eleifend nulla mattis mauris dignissim nec sollicitudin lacinia per hac erat sapien maximus sagittis potenti odio inceptos penatibus elit dictumst purus pretium imperdiet sem a fringilla leo magnis ornare dolor amet finibus lectus quis sed sit adipiscing torquent tempus dapibus pellentesque velit consectetur viverra blandit massa primis eros nullam duis litora nascetur diam etiam vivamus senectus pulvinar quisque condimentum vehicula semper habitasse curae aptent augue suscipit ullamcorper nisl arcu netus', '2019-11-25'),
(28, 'augue purus aliquet nulla', 'fames himenaeos neque mauris et adipiscing taciti vitae pellentesque vestibulum dictumst quam cras proin netus ridiculus ex habitant ornare ligula platea luctus pharetra ut sapien torquent quis maximus nam tellus faucibus risus consectetur ultrices vehicula eleifend interdum eros turpis lacus augue parturient elit fringilla aenean ullamcorper molestie condimentum consequat bibendum amet sem felis curabitur leo enim egestas nisl rhoncus facilisi volutpat urna nostra porta sagittis scelerisque est montes donec habitasse dolor lacinia odio accumsan nunc eu quisque ad cursus lectus tempor malesuada vulputate facilisis gravida eget feugiat sociosqu primis id suscipit porttitor sit tempus fermentum dis in hendrerit integer justo', '2019-11-25'),
(29, 'adipiscing interdum hendrerit malesuada', 'mollis nostra turpis nunc elementum ut rutrum vitae facilisis interdum orci leo inceptos maecenas posuere mi montes sagittis pellentesque velit hac curabitur convallis ultrices platea at donec est pulvinar parturient aliquet morbi vivamus mauris habitant quis sodales duis malesuada aptent lorem suscipit lectus fusce venenatis class a fermentum nisl lobortis iaculis arcu non consequat eros efficitur bibendum per feugiat phasellus augue ornare semper taciti risus ultricies faucibus laoreet nam vehicula tempor ridiculus commodo ipsum volutpat nisi penatibus himenaeos fringilla dis gravida odio curae metus placerat quisque eget varius pretium luctus aenean ex tellus sed vel urna tincidunt ac neque netus', '2019-11-25'),
(30, 'elit eros sollicitudin maecenas', 'odio cubilia non vestibulum mollis nullam fermentum porttitor viverra auctor cursus elementum ut quis risus lacinia purus aliquam aenean mattis sed dictum id enim suspendisse sit duis consectetur curae imperdiet nisl facilisi nec tristique pharetra convallis iaculis maecenas semper commodo accumsan finibus inceptos metus montes dapibus curabitur cras est ac per aptent velit arcu rhoncus orci maximus hendrerit conubia parturient quam nam taciti vehicula justo platea ullamcorper faucibus sapien vivamus scelerisque class neque fames ridiculus in euismod ultrices condimentum ex ad interdum malesuada tincidunt lacus mus tortor nascetur vel dignissim erat primis magnis facilisis nulla consequat porta placerat urna augue', '2019-11-25'),
(31, 'in adipiscing tristique ipsum', 'habitasse suscipit consectetur a parturient aliquam litora commodo fringilla convallis enim interdum ad sociosqu aliquet nibh nunc ut senectus eleifend montes neque elit pretium et hac eros rhoncus turpis inceptos sem adipiscing facilisis molestie aptent gravida netus libero per sollicitudin lobortis efficitur dis ultricies lacus mollis leo sapien varius maecenas ligula amet facilisi porttitor justo augue purus vel cubilia volutpat phasellus ante nisi integer nulla venenatis consequat ullamcorper felis lorem taciti metus primis velit mauris donec dolor congue nullam eget placerat quam platea mus risus vitae hendrerit scelerisque in condimentum sodales porta diam etiam urna accumsan vestibulum orci lacinia dictum', '2019-11-25'),
(32, 'sem risus pharetra ultrices', 'habitasse augue in venenatis accumsan nostra velit felis potenti nisi facilisis blandit himenaeos maximus nunc urna amet eu malesuada torquent arcu sed ex turpis purus parturient vivamus sapien sagittis curae eget aliquet scelerisque donec eleifend diam quis dictumst gravida cursus iaculis efficitur tortor a consequat ultrices enim ridiculus ornare ipsum tempus est ac finibus aenean molestie curabitur nam consectetur pretium nisl orci tempor sem nec lacinia vestibulum class rutrum taciti magnis adipiscing sit feugiat tristique bibendum tellus tincidunt dictum etiam habitant eros mollis convallis pharetra fames fermentum suspendisse commodo inceptos vel id neque aliquam penatibus condimentum egestas mauris faucibus per', '2019-11-25'),
(33, 'duis venenatis efficitur volutpat', 'pretium habitant habitasse porttitor facilisi sit molestie malesuada est vel nulla massa taciti eros imperdiet duis neque inceptos nisl sodales placerat nostra dui leo sed eleifend torquent aliquet tempor non curae consequat metus class feugiat sollicitudin nec interdum ultrices amet tortor parturient ad nisi fermentum posuere vulputate mus auctor ridiculus mollis himenaeos ac mattis magnis mi gravida potenti efficitur dis ultricies ut dolor laoreet montes sociosqu netus ex hac porta iaculis nullam justo nascetur vitae ullamcorper penatibus orci nunc libero elit a elementum commodo lacinia vestibulum aenean hendrerit eget fames urna nibh cursus tincidunt erat lacus bibendum platea sem ipsum', '2019-11-25'),
(34, 'donec pulvinar netus felis', 'varius pretium bibendum velit leo ad facilisis condimentum venenatis semper magnis tempus libero posuere turpis sed amet conubia a dignissim et dictum felis nulla tortor primis sem non nisl mollis nibh dui vel penatibus senectus morbi montes pellentesque torquent parturient maximus commodo platea cursus hac convallis nisi eleifend ullamcorper porttitor ridiculus accumsan massa habitant ultrices consectetur orci nullam dis vitae feugiat nam tempor scelerisque praesent auctor quam sollicitudin ut arcu luctus enim mauris molestie eget aenean laoreet at potenti gravida vivamus pharetra fames netus taciti ligula dapibus purus mus lectus rhoncus pulvinar interdum rutrum egestas sapien magna proin volutpat suspendisse', '2019-11-25'),
(35, 'dis semper scelerisque taciti', 'congue quisque purus morbi facilisis mattis ex habitasse pulvinar sed sagittis vulputate ultrices fermentum natoque tincidunt sem taciti ante nibh magna velit vitae neque vehicula turpis netus euismod fusce amet pharetra fringilla auctor egestas praesent efficitur luctus convallis lacus dui imperdiet montes torquent consequat venenatis dis lectus porttitor conubia eros feugiat at felis semper tellus risus ridiculus rhoncus facilisi metus tristique laoreet porta inceptos duis hac per mus magnis penatibus dolor gravida iaculis elementum fames eu ornare nisi potenti primis condimentum curabitur augue maecenas est habitant in sapien finibus viverra urna vel aptent justo litora bibendum lorem nunc nisl class', '2019-11-25'),
(36, 'sapien fames finibus mi', 'maximus dolor elementum urna suscipit fames sociosqu ligula consectetur cursus hendrerit justo semper gravida dis sollicitudin egestas potenti eget faucibus sit vehicula parturient suspendisse risus purus eu neque convallis cras non mi diam ullamcorper imperdiet magnis iaculis mollis dictum proin commodo elit metus magna nibh ad curabitur sagittis nisl leo efficitur laoreet phasellus cubilia ac rhoncus fringilla mattis per volutpat turpis vivamus maecenas tincidunt class libero fusce bibendum dictumst luctus fermentum odio penatibus etiam natoque ex himenaeos consequat venenatis nulla et primis tristique mus arcu congue ultricies posuere conubia porta netus sodales inceptos accumsan facilisi amet est erat donec nam', '2019-11-25'),
(37, 'arcu dictum mattis accumsan', 'amet ante in rhoncus natoque mus potenti vitae tortor phasellus sit risus elit odio primis habitant cras per fames lectus accumsan quisque cursus parturient ornare vivamus dolor ligula eget diam nunc scelerisque placerat quis ullamcorper aliquam posuere dis pretium sagittis hendrerit ultrices bibendum finibus eros nulla conubia integer faucibus sapien inceptos elementum pharetra aliquet condimentum curabitur congue egestas vel porta dapibus lobortis iaculis platea pellentesque ad ridiculus ultricies suscipit interdum adipiscing sociosqu fringilla justo praesent malesuada augue non libero dictumst commodo nostra vulputate ipsum magnis facilisi imperdiet feugiat at curae sollicitudin quam est tincidunt fusce enim mauris volutpat nisi tristique', '2019-11-25'),
(38, 'curae tellus volutpat pellentesque', 'sem pharetra hac eget fames fermentum litora nisi habitant dapibus montes nunc est vulputate pretium proin risus curabitur cubilia ultrices taciti ornare mattis dolor lacinia felis ut vel magna suscipit in per curae finibus massa quam torquent porta posuere nisl convallis faucibus phasellus bibendum gravida duis potenti blandit dictum dis suspendisse morbi consequat sed eu senectus laoreet himenaeos magnis ex primis sit nec efficitur tincidunt ad fringilla sociosqu semper varius sodales maecenas tristique interdum mollis mus luctus nascetur nostra ipsum class vestibulum lobortis lorem facilisi tempor vehicula natoque enim molestie quis venenatis maximus etiam praesent aenean id dui sollicitudin aptent', '2019-11-25'),
(39, 'eleifend vivamus curae natoque', 'mauris donec semper fermentum malesuada curae aenean nibh sodales orci penatibus venenatis nostra cursus etiam nunc arcu sagittis nisl mattis eget massa quam in habitasse vitae dignissim ridiculus hendrerit hac torquent nullam non ligula nec justo consectetur dui ultrices magnis ex dis porttitor facilisis eleifend adipiscing elementum montes vulputate ante ad faucibus et lacinia dictum viverra urna suspendisse amet vivamus feugiat cras diam aliquam suscipit posuere sociosqu efficitur sollicitudin sem vel parturient tristique nulla mi odio euismod ac platea finibus nam pellentesque id a accumsan aliquet praesent fringilla vestibulum pharetra velit nisi lorem taciti mollis rutrum tortor phasellus placerat purus', '2019-11-25'),
(40, 'facilisi suscipit convallis tempus', 'elit urna maecenas accumsan lobortis mauris dictum potenti aliquet euismod neque odio cubilia cursus facilisis cras tempor rutrum himenaeos dictumst elementum purus ante non magna metus praesent vulputate lacus faucibus orci libero sagittis felis semper velit ridiculus litora ultricies scelerisque tempus bibendum placerat diam class maximus enim mattis ipsum phasellus sit parturient natoque quam dui fusce habitant etiam ac conubia nostra lorem torquent massa porta pulvinar facilisi nunc donec sodales vitae nam hendrerit fringilla mi dapibus leo dolor in sem nibh taciti efficitur adipiscing nec vivamus senectus netus montes aenean tincidunt tellus eleifend pellentesque dis vel auctor inceptos molestie nisl', '2019-11-25'),
(41, 'ex phasellus magna class', 'neque montes bibendum orci fringilla per netus pellentesque senectus platea ex nec gravida tempus ad tellus vestibulum nunc auctor enim dolor donec taciti mi fermentum feugiat arcu dui leo malesuada egestas euismod rhoncus vulputate urna dis integer vitae non pulvinar sed sociosqu purus facilisi lacus conubia suscipit lectus fames habitasse suspendisse commodo quam dignissim ipsum faucibus ante erat nullam imperdiet porta cras vel potenti sem turpis diam interdum curae nisl pharetra massa mattis eleifend aptent lorem eget fusce et odio tincidunt hendrerit mus ridiculus lobortis viverra risus volutpat mollis ligula dictumst sapien mauris nam vivamus est posuere litora id ultrices', '2019-11-25'),
(42, 'dui et suscipit nulla', 'scelerisque quisque cras senectus phasellus natoque interdum proin lacus aenean sociosqu cubilia litora eget sem neque per montes suspendisse ex amet tincidunt suscipit risus nascetur pellentesque nostra hac feugiat lectus accumsan urna fringilla tristique mus enim netus adipiscing volutpat dolor ultricies consectetur dictumst convallis metus arcu aliquet dapibus dis velit finibus rhoncus commodo quam eleifend primis gravida massa ultrices orci duis sit pharetra consequat dignissim nibh viverra ut efficitur fames ad potenti sagittis dictum nisi etiam porta facilisis tempus conubia nullam ullamcorper sed curae magna id est habitant blandit vulputate ornare congue donec nunc vehicula magnis lobortis libero egestas varius', '2019-11-25'),
(43, 'nullam neque curae sagittis', 'nullam ac molestie lacus sociosqu curabitur tristique proin gravida placerat tempor eleifend finibus vivamus suspendisse maecenas et sapien phasellus pretium justo volutpat vestibulum diam efficitur vitae erat libero dapibus habitant etiam quisque venenatis praesent cras dictumst in commodo blandit hendrerit inceptos fusce consequat lorem malesuada condimentum quis mattis nisi dui augue bibendum ullamcorper interdum hac dictum scelerisque a vehicula torquent luctus tempus ex ipsum eu ante varius penatibus iaculis porta id auctor posuere lobortis potenti eget litora porttitor mauris fermentum urna dignissim amet tincidunt felis quam facilisi orci aptent nascetur consectetur pulvinar integer sodales velit elit primis rutrum aenean adipiscing', '2019-11-25'),
(44, 'habitasse non egestas volutpat', 'mus habitasse placerat ad tempor etiam donec libero potenti consequat varius maximus metus phasellus curae suscipit aliquam feugiat fringilla sed erat ultricies nulla quam vestibulum aenean aliquet molestie pharetra nibh primis natoque urna dolor elit rhoncus purus egestas ultrices platea massa facilisis ornare ante litora himenaeos eleifend tellus quis gravida pellentesque mauris ridiculus euismod ac arcu vulputate in semper sagittis nam ex felis ullamcorper convallis morbi vivamus a neque lacus praesent leo fusce et turpis nunc vel nisi luctus eget habitant tortor nec blandit sit cursus sociosqu mi magnis lacinia velit integer efficitur faucibus lectus cubilia ut montes sollicitudin dictumst', '2019-11-25'),
(45, 'neque mauris vivamus potenti', 'luctus molestie ultricies sit mattis tristique enim dolor neque morbi purus cubilia ad ultrices proin tincidunt libero dictumst habitant ante sed auctor diam platea ac litora himenaeos natoque curae arcu odio vehicula cras penatibus facilisi pretium ridiculus leo vulputate tellus turpis convallis nam rhoncus praesent venenatis phasellus senectus aliquet elit ut faucibus fringilla mollis class tempus suspendisse cursus augue finibus donec interdum aliquam integer quam sem et lacus netus ipsum taciti duis consectetur scelerisque aptent a vestibulum erat lobortis condimentum eros vivamus magna velit sagittis nisl placerat quis nibh massa est congue varius urna nulla primis eget id rutrum nostra', '2019-11-25'),
(46, 'luctus dictumst rutrum curabitur', 'aenean primis lacus interdum facilisi praesent nunc phasellus per cras litora pharetra diam eu mus nulla scelerisque fermentum molestie ultrices pretium aliquet nisi commodo dictum justo turpis quis odio cubilia leo pulvinar tempus maximus dui ornare porttitor magnis integer tellus sagittis iaculis netus convallis etiam blandit velit malesuada penatibus urna inceptos varius purus et augue tincidunt dictumst elementum venenatis imperdiet efficitur quam felis fusce tempor libero risus finibus dignissim tortor lacinia vitae habitant eleifend himenaeos natoque maecenas quisque nibh fringilla suspendisse neque eros lobortis posuere suscipit gravida hendrerit ac non placerat sem erat consectetur ridiculus est cursus conubia nec facilisis', '2019-11-25'),
(47, 'dis morbi ad sit', 'elementum auctor suscipit tristique dolor id nascetur morbi egestas lobortis mattis leo aliquam eleifend litora aliquet fringilla nunc tempor placerat arcu platea lacus orci ad porta quisque sed vivamus taciti vitae blandit conubia nisl nisi aenean velit mi quis ligula accumsan porttitor sem eros volutpat cursus fusce at turpis dui justo tortor enim curae luctus faucibus commodo fames sagittis nibh euismod finibus congue vestibulum facilisis ultricies per nullam lorem rutrum pellentesque vulputate malesuada diam ridiculus et nec ultrices posuere convallis rhoncus in ex non pulvinar felis donec lacinia amet varius ornare mollis penatibus dignissim odio pretium mauris ante mus bibendum', '2019-11-25'),
(48, 'amet sem convallis hendrerit', 'tincidunt auctor fringilla venenatis tortor augue dignissim risus himenaeos mus adipiscing praesent egestas accumsan turpis nunc hendrerit vivamus semper habitasse fermentum volutpat rutrum porta fames urna finibus dis parturient ex taciti placerat nascetur laoreet congue aptent malesuada id odio facilisis pretium eget pulvinar interdum lorem at libero metus penatibus cursus senectus commodo netus a lacinia ullamcorper torquent morbi erat ridiculus primis mollis per tellus suspendisse dictum justo posuere mi ornare nostra aliquam felis viverra lectus potenti feugiat quis consectetur sed luctus eu elementum sodales vestibulum tristique ac sociosqu enim fusce mattis convallis mauris facilisi ultrices bibendum class amet faucibus orci', '2019-11-25'),
(49, 'commodo tristique nec maximus', 'risus eu luctus gravida vivamus sed potenti odio neque adipiscing natoque posuere tincidunt vulputate eros interdum torquent vestibulum aptent ligula suspendisse rutrum dapibus vitae nam ridiculus magna metus faucibus blandit donec turpis per erat pellentesque molestie praesent bibendum felis condimentum nisl fusce leo massa quis primis porttitor maximus vehicula dolor in mi eget justo nibh parturient malesuada commodo id fames platea dictumst class ad non egestas et litora penatibus curae phasellus eleifend aliquet velit ut dignissim sociosqu hac magnis fermentum aliquam volutpat orci finibus congue habitant ultricies mus viverra ornare iaculis lorem himenaeos feugiat varius hendrerit rhoncus nunc cras at', '2019-11-25'),
(50, 'viverra nullam quis bibendum', 'habitasse metus per purus class sapien cubilia platea integer orci odio donec viverra vivamus nisi libero at pellentesque congue eros rutrum tellus posuere leo semper ex amet fames volutpat sociosqu dictum porttitor justo aenean ridiculus hendrerit dolor a nunc blandit elementum fringilla arcu dapibus enim massa convallis elit praesent luctus nam tempor penatibus lorem lacinia phasellus accumsan iaculis vitae et habitant potenti pharetra gravida quisque eget facilisi mauris risus porta nullam aliquet duis sed primis maecenas ut felis montes non finibus mollis proin ullamcorper hac fermentum dictumst mattis vel dignissim venenatis vulputate euismod in diam id suspendisse senectus pulvinar sodales', '2019-11-25');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cid` int(11) NOT NULL,
  `categories1` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cid`, `categories1`) VALUES
(4, 'books'),
(3, 'cinema'),
(6, 'hindi'),
(1, 'india'),
(2, 'kerala'),
(5, 'malayalam');

-- --------------------------------------------------------

--
-- Table structure for table `reltab`
--

CREATE TABLE `reltab` (
  `blogid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reltab`
--

INSERT INTO `reltab` (`blogid`, `tagid`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(4, 7),
(4, 8),
(5, 9),
(5, 10),
(6, 11),
(6, 12),
(7, 13),
(7, 14),
(8, 15),
(8, 16),
(9, 17),
(9, 18),
(10, 19),
(10, 20),
(11, 3),
(11, 21),
(12, 22),
(12, 23),
(13, 24),
(13, 23),
(14, 25),
(14, 26),
(15, 27),
(15, 28),
(16, 29),
(16, 30),
(17, 31),
(17, 6),
(18, 32),
(18, 33),
(19, 32),
(19, 27),
(20, 34),
(20, 35),
(21, 26),
(21, 36),
(22, 34),
(22, 37),
(23, 38),
(23, 39),
(24, 40),
(24, 39),
(25, 2),
(25, 41),
(26, 42),
(26, 37),
(27, 43),
(27, 2),
(28, 44),
(28, 45),
(29, 46),
(29, 47),
(30, 48),
(30, 49),
(31, 50),
(31, 51),
(32, 52),
(32, 53),
(33, 1),
(33, 4),
(34, 54),
(34, 45),
(35, 55),
(35, 38),
(36, 56),
(36, 34),
(37, 19),
(37, 57),
(38, 58),
(38, 54),
(39, 59),
(39, 60),
(40, 25),
(40, 61),
(41, 62),
(41, 63),
(42, 64),
(42, 65),
(43, 66),
(43, 67),
(44, 68),
(44, 69),
(45, 70),
(45, 71),
(46, 72),
(46, 73),
(47, 3),
(47, 34),
(48, 74),
(48, 71),
(49, 75),
(49, 28),
(50, 76),
(50, 71);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `tid` int(11) NOT NULL,
  `tags` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`tid`, `tags`) VALUES
(11, 'ac'),
(37, 'ad'),
(15, 'adipiscing'),
(58, 'aenean'),
(53, 'aliquam'),
(73, 'ante'),
(61, 'arcu'),
(66, 'augue'),
(16, 'bibendum'),
(5, 'blandit'),
(44, 'commodo'),
(51, 'conubia'),
(29, 'convallis'),
(34, 'cubilia'),
(17, 'dapibus'),
(68, 'dictumst'),
(55, 'dis'),
(36, 'duis'),
(50, 'egestas'),
(6, 'enim'),
(9, 'euismod'),
(20, 'facilisi'),
(72, 'fermentum'),
(43, 'fringilla'),
(25, 'fusce'),
(13, 'habitasse'),
(22, 'hendrerit'),
(30, 'himenaeos'),
(18, 'iaculis'),
(60, 'imperdiet'),
(56, 'interdum'),
(1, 'justo'),
(42, 'lacinia'),
(38, 'lectus'),
(27, 'leo'),
(64, 'libero'),
(71, 'litora'),
(32, 'lobortis'),
(41, 'maecenas'),
(69, 'metus'),
(4, 'molestie'),
(12, 'montes'),
(2, 'nec'),
(45, 'nibh'),
(26, 'nisl'),
(19, 'non'),
(49, 'nulla'),
(23, 'nullam'),
(35, 'nunc'),
(54, 'odio'),
(62, 'orci'),
(46, 'parturient'),
(67, 'penatibus'),
(59, 'pharetra'),
(52, 'phasellus'),
(24, 'platea'),
(33, 'porttitor'),
(31, 'posuere'),
(48, 'potenti'),
(70, 'proin'),
(75, 'risus'),
(8, 'sapien'),
(76, 'semper'),
(40, 'sit'),
(57, 'sociosqu'),
(39, 'sollicitudin'),
(3, 'suscipit'),
(74, 'turpis'),
(21, 'ultrices'),
(10, 'ultricies'),
(65, 'urna'),
(7, 'ut'),
(28, 'varius'),
(47, 'vehicula'),
(14, 'velit'),
(63, 'vitae');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cid`),
  ADD UNIQUE KEY `categories1` (`categories1`);

--
-- Indexes for table `reltab`
--
ALTER TABLE `reltab`
  ADD KEY `blogid` (`blogid`),
  ADD KEY `tagid` (`tagid`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`tid`),
  ADD UNIQUE KEY `tags` (`tags`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `reltab`
--
ALTER TABLE `reltab`
  ADD CONSTRAINT `reltab_ibfk_1` FOREIGN KEY (`blogid`) REFERENCES `blog` (`bid`),
  ADD CONSTRAINT `reltab_ibfk_2` FOREIGN KEY (`tagid`) REFERENCES `tag` (`tid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
